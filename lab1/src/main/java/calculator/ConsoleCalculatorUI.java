package calculator;

import calculator.operations.Operation;
import calculator.operations.factory.OperationFactory;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleCalculatorUI implements CalculatorUI {
    private final Scanner scanner = new Scanner(System.in);
    private final Calculator calculator;
    private final OperationFactory operationFactory;

    public ConsoleCalculatorUI(Calculator calculator, OperationFactory operationFactory) {
        this.calculator = calculator;
        this.operationFactory = operationFactory;
    }

    @Override
    public double readNumber() {
        double number = scanner.nextDouble();
        scanner.nextLine();

        return number;
    }

    @Override
    public Operation readOperation() {
        String operation = scanner.nextLine().trim();

        return operationFactory.createOperation(operation);
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("\nCurrent result: " + String.format("%.4f", calculator.getResult()));

            try {
                System.out.print("Pick an operation (+,-,*,/,min,max,sqrt): ");
                Operation operation = readOperation();

                System.out.print("Type a number: ");
                double number = readNumber();

                calculator.compute(operation, number);
            } catch (InputMismatchException e) {
                System.out.println("Invalid number");
                scanner.nextLine();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
