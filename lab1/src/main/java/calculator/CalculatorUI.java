package calculator;

import calculator.operations.Operation;

public interface CalculatorUI {
    double readNumber();
    Operation readOperation();
    void run();
}
