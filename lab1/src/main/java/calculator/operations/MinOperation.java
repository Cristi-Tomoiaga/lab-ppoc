package calculator.operations;

public class MinOperation implements Operation {
    @Override
    public double evaluate(double a, double b) {
        return Math.min(a, b);
    }
}
