package calculator.operations;

public class MultiplicationOperation implements Operation {
    @Override
    public double evaluate(double a, double b) {
        return a * b;
    }
}
