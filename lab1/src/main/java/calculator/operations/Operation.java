package calculator.operations;

public interface Operation {
    double evaluate(double a, double b) throws OperationException;
}
