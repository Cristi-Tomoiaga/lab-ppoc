package calculator.operations;

public class DivisionOperation implements Operation {
    @Override
    public double evaluate(double a, double b) throws OperationException {
        if (b == 0.0) {
            throw new OperationException("Second number must not be 0");
        }

        return a / b;
    }
}
