package calculator.operations;

public class AdditionOperation implements Operation {
    @Override
    public double evaluate(double a, double b) {
        return a + b;
    }
}
