package calculator.operations.factory;

import calculator.operations.Operation;

public interface OperationFactory {
    Operation createOperation(String operation);
}
