package calculator.operations.factory;

import calculator.operations.*;

public class OperationFactoryImpl implements OperationFactory {
    @Override
    public Operation createOperation(String operation) {
        switch (operation.trim()) {
            case "+":
                return new AdditionOperation();
            case "-":
                return new SubtractionOperation();
            case "*":
                return new MultiplicationOperation();
            case "/":
                return new DivisionOperation();
            case "min":
                return new MinOperation();
            case "max":
                return new MaxOperation();
            case "sqrt":
                return new SqrtOperation();
            default:
                throw new IllegalArgumentException("Operation not permitted");
        }
    }
}
