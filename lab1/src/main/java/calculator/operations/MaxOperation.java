package calculator.operations;

public class MaxOperation implements Operation {
    @Override
    public double evaluate(double a, double b) {
        return Math.max(a, b);
    }
}
