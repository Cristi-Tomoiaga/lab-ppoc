package calculator.operations;

public class SubtractionOperation implements Operation {
    @Override
    public double evaluate(double a, double b) {
        return a - b;
    }
}
