package calculator.operations;

public class SqrtOperation implements Operation {
    @Override
    public double evaluate(double a, double b) throws OperationException {
        if (a < 0.0) {
            throw new OperationException("First number must be positive");
        }

        if (b <= 1.0) {
            throw new OperationException("Second number must be bigger than 1");
        }

        return Math.pow(a, 1.0/b);
    }
}
