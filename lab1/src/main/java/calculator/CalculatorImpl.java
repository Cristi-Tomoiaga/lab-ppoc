package calculator;

import calculator.operations.Operation;
import calculator.operations.OperationException;

public class CalculatorImpl implements Calculator {
    private double result;

    public CalculatorImpl() {
        this.result = 0;
    }

    @Override
    public void compute(Operation operation, double number) throws OperationException {
        result = operation.evaluate(result, number);
    }

    @Override
    public double getResult() {
        return result;
    }
}
