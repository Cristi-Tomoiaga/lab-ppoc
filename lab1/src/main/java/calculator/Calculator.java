package calculator;

import calculator.operations.Operation;
import calculator.operations.OperationException;

public interface Calculator {
    void compute(Operation operation, double number) throws OperationException;
    double getResult();
}
