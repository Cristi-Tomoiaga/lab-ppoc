import calculator.Calculator;
import calculator.CalculatorImpl;
import calculator.CalculatorUI;
import calculator.ConsoleCalculatorUI;
import calculator.operations.factory.OperationFactory;
import calculator.operations.factory.OperationFactoryImpl;

public class Main {
    public static void main(String[] args) {
        OperationFactory operationFactory = new OperationFactoryImpl();
        Calculator calculator = new CalculatorImpl();
        CalculatorUI calculatorUI = new ConsoleCalculatorUI(calculator, operationFactory);

        calculatorUI.run();
    }
}
