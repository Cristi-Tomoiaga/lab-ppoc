package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxOperationTest {
    private Operation max;

    @BeforeEach
    void setUp() {
        max = new MaxOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> max.evaluate(3.0, 2.5));
        assertEquals(3.0, result);

        result = assertDoesNotThrow(() -> max.evaluate(3.0, 3.0));
        assertEquals(3.0, result);

        result = assertDoesNotThrow(() -> max.evaluate(-4.5, 15));
        assertEquals(15, result);
    }
}