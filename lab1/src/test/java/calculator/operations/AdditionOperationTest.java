package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AdditionOperationTest {
    private Operation addition;

    @BeforeEach
    void setUp() {
        addition = new AdditionOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> addition.evaluate(1.0, 2.0));
        assertEquals(3.0, result);

        result = assertDoesNotThrow(() -> addition.evaluate(2.0, 0.0));
        assertEquals(2.0, result);

        result = assertDoesNotThrow(() -> addition.evaluate(-2.0, 0.0));
        assertEquals(-2.0, result);
    }
}