package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinOperationTest {
    private Operation min;

    @BeforeEach
    void setUp() {
        min = new MinOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> min.evaluate(3.0, 2.5));
        assertEquals(2.5, result);

        result = assertDoesNotThrow(() -> min.evaluate(3.0, 3.0));
        assertEquals(3.0, result);

        result = assertDoesNotThrow(() -> min.evaluate(-4.5, 15));
        assertEquals(-4.5, result);
    }
}