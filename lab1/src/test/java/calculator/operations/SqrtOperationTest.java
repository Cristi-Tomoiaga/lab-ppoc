package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SqrtOperationTest {
    private Operation sqrt;

    @BeforeEach
    void setUp() {
        sqrt = new SqrtOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> sqrt.evaluate(16, 2.0));
        assertEquals(4.0, result);

        result = assertDoesNotThrow(() -> sqrt.evaluate(8.0, 3));
        assertEquals(2, result);

        result = assertDoesNotThrow(() -> sqrt.evaluate(27, 3.0));
        assertEquals(3, result);
    }

    @Test
    void testEvaluateInvalid() {
        OperationException exception = assertThrows(OperationException.class, () -> sqrt.evaluate(1.0, -3.0));
        assertEquals("Second number must be bigger than 1", exception.getMessage());

        exception = assertThrows(OperationException.class, () -> sqrt.evaluate(1.0, 1.0));
        assertEquals("Second number must be bigger than 1", exception.getMessage());

        exception = assertThrows(OperationException.class, () -> sqrt.evaluate(-1.0, 3.0));
        assertEquals("First number must be positive", exception.getMessage());
    }
}