package calculator.operations.factory;

import calculator.operations.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperationFactoryImplTest {
    private OperationFactory factory;

    @BeforeEach
    void setUp() {
        factory = new OperationFactoryImpl();
    }

    @Test
    void testCreateOperation() {
        Operation operation = assertDoesNotThrow(() -> factory.createOperation("+"));
        assertInstanceOf(AdditionOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation("sqrt"));
        assertInstanceOf(SqrtOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation("min"));
        assertInstanceOf(MinOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation("max"));
        assertInstanceOf(MaxOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation("*"));
        assertInstanceOf(MultiplicationOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation("-"));
        assertInstanceOf(SubtractionOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation("/"));
        assertInstanceOf(DivisionOperation.class, operation);

        operation = assertDoesNotThrow(() -> factory.createOperation(" /  "));
        assertInstanceOf(DivisionOperation.class, operation);
    }

    @Test
    void testCreateOperationInvalid() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> factory.createOperation("nothing"));
        assertEquals("Operation not permitted", exception.getMessage());
    }
}