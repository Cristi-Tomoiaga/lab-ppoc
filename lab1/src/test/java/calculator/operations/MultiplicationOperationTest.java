package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationOperationTest {
    private Operation multiplication;

    @BeforeEach
    void setUp() {
        multiplication = new MultiplicationOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> multiplication.evaluate(4.0, 2.0));
        assertEquals(8.0, result);

        result = assertDoesNotThrow(() -> multiplication.evaluate(-4.0, 1.0));
        assertEquals(-4.0, result);

        result = assertDoesNotThrow(() -> multiplication.evaluate(13.0, 0));
        assertEquals(0, result);
    }
}