package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubtractionOperationTest {
    private Operation subtraction;

    @BeforeEach
    void setUp() {
        subtraction = new SubtractionOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> subtraction.evaluate(-4.0, 1.0));
        assertEquals(-5.0, result);

        result = assertDoesNotThrow(() -> subtraction.evaluate(10, 1.0));
        assertEquals(9, result);

        result = assertDoesNotThrow(() -> subtraction.evaluate(-4.0, 0));
        assertEquals(-4.0, result);

        result = assertDoesNotThrow(() -> subtraction.evaluate(4.0, 4));
        assertEquals(0, result);
    }
}