package calculator.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivisionOperationTest {
    private Operation division;

    @BeforeEach
    void setUp() {
        division = new DivisionOperation();
    }

    @Test
    void testEvaluate() {
        double result = assertDoesNotThrow(() -> division.evaluate(4.0, 2.0));
        assertEquals(2.0, result);

        result = assertDoesNotThrow(() -> division.evaluate(1.5, 3));
        assertEquals(0.5, result);

        result = assertDoesNotThrow(() -> division.evaluate(-6.0, 1.0));
        assertEquals(-6.0, result);
    }

    @Test
    void testEvaluateInvalid() {
        OperationException exception = assertThrows(OperationException.class, () -> division.evaluate(1.0, 0.0));
        assertEquals("Second number must not be 0", exception.getMessage());
    }
}