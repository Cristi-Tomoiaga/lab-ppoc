package calculator;

import calculator.operations.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorImplTest {
    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new CalculatorImpl();
    }

    @Test
    void testComputeGetResult() {
        assertEquals(0.0, calculator.getResult());

        assertDoesNotThrow(() -> calculator.compute(new AdditionOperation(), 12.4));
        assertEquals(12.4, calculator.getResult());

        assertDoesNotThrow(() -> calculator.compute(new DivisionOperation(), 2));
        assertEquals(6.2, calculator.getResult());

        assertDoesNotThrow(() -> calculator.compute(new MaxOperation(), 16));
        assertEquals(16, calculator.getResult());

        assertDoesNotThrow(() -> calculator.compute(new SqrtOperation(), 2));
        assertEquals(4, calculator.getResult());
    }

    @Test
    void testComputeGetResultInvalid() {
        assertEquals(0.0, calculator.getResult());

        assertThrows(OperationException.class, () -> calculator.compute(new DivisionOperation(), 0.0));
        assertEquals(0.0, calculator.getResult());

        assertThrows(OperationException.class, () -> calculator.compute(new SqrtOperation(), 1.0));
        assertEquals(0.0, calculator.getResult());
    }
}