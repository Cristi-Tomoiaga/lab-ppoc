package repository;

import model.Identifiable;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Identifiable & Comparable<T>> implements InMemoryRepository<T> {
    private final Set<T> set = new TreeSet<>();

    @Override
    public void add(T entity) {
        set.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
