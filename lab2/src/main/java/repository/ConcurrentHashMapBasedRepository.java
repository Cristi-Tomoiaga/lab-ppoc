package repository;

import model.Identifiable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final Map<Integer, T> map = new ConcurrentHashMap<>();

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId()) && map.get(entity.getId()).equals(entity);
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }

    @Override
    public void clear() {
        map.clear();
    }
}
