package repository;

import model.Identifiable;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final List<T> list = new ArrayList<>();

    @Override
    public void add(T entity) {
        list.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return list.contains(entity);
    }

    @Override
    public void remove(T entity) {
        list.remove(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
