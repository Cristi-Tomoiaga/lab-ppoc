package repository;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import model.Identifiable;

public class TIntObjectMapBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final TIntObjectMap<T> map = new TIntObjectHashMap<>();

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId()) && map.get(entity.getId()).equals(entity);
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }

    @Override
    public void clear() {
        map.clear();
    }
}
