package repository;

import model.Identifiable;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final Set<T> set = new HashSet<>();

    @Override
    public void add(T entity) {
        set.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return set.remove(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
