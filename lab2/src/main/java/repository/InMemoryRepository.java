package repository;

import model.Identifiable;

public interface InMemoryRepository<T extends Identifiable> {
    void add(T entity);
    boolean contains(T entity);
    void remove(T entity);
    void clear();
}
