package repository;

import model.Identifiable;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Set;

public class UnifiedSetBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final Set<T> set = UnifiedSet.newSet();

    @Override
    public void add(T entity) {
        set.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
