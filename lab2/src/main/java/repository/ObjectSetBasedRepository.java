package repository;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import model.Identifiable;

public class ObjectSetBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final ObjectSet<T> set = new ObjectOpenHashSet<>();

    @Override
    public void add(T entity) {
        set.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
