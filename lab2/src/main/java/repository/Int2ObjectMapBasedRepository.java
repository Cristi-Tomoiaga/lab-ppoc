package repository;

import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import model.Identifiable;

public class Int2ObjectMapBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final Int2ObjectMap<T> map = new Int2ObjectAVLTreeMap<>();

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId()) && map.get(entity.getId()).equals(entity);
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }

    @Override
    public void clear() {
        map.clear();
    }
}
