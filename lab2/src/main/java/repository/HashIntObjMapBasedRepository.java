package repository;

import com.koloboke.collect.map.hash.HashIntObjMap;
import com.koloboke.collect.map.hash.HashIntObjMaps;
import model.Identifiable;

import java.util.Objects;

public class HashIntObjMapBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final HashIntObjMap<T> map = HashIntObjMaps.newMutableMap();

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId()) && Objects.equals(map.get(entity.getId()), entity);
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }

    @Override
    public void clear() {
        map.clear();
    }
}
