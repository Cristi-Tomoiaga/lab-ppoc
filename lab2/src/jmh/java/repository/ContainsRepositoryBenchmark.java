package repository;

import model.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@Fork(1)
@Measurement(iterations = 5, time = 5)
@Warmup(iterations = 2, time = 5)
@OutputTimeUnit(TimeUnit.SECONDS)
public class ContainsRepositoryBenchmark {
    @State(Scope.Benchmark)
    public static class OrdersState {
        @Param({"1000", "10000"})
        public int size;

        public List<Order> orders = new ArrayList<>();

        @Setup(Level.Trial)
        public void doSetup() {
            Random random = new Random();

            orders.clear();
            for (int i = 0; i < size; i++) {
                orders.add(new Order(i, random.nextInt(), random.nextInt()));
            }

            Collections.shuffle(orders);
        }
    }

    @State(Scope.Benchmark)
    public static class RepoState {
        @Param({"ARRAY_LIST", "HASH_SET", "TREE_SET", "CONCURRENT_HASH_MAP",
                "UNIFIED_SET", "OBJECT_SET", "INT2OBJECT_MAP", "TINT_OBJECT_MAP", "HASH_INT_OBJ_MAP"})
        public RepositoryType repositoryType;

        public InMemoryRepository<Order> repository;

        @Setup(Level.Trial)
        public void doSetup(OrdersState ordersState) {
            repository = repositoryType.supplier().get();

            for (Order order: ordersState.orders) {
                repository.add(order);
            }
        }
    }

    @Benchmark
    public void testContainsOperation(Blackhole bh, OrdersState ordersState, RepoState repoState) {
        for (Order order: ordersState.orders) {
            bh.consume(repoState.repository.contains(order));
        }
    }
}
