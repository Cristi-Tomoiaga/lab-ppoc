package repository;

import model.Order;

import java.util.function.Supplier;

public enum RepositoryType {
    ARRAY_LIST(ArrayListBasedRepository::new),
    HASH_SET(HashSetBasedRepository::new),
    TREE_SET(TreeSetBasedRepository::new),
    CONCURRENT_HASH_MAP(ConcurrentHashMapBasedRepository::new),
    UNIFIED_SET(UnifiedSetBasedRepository::new),
    OBJECT_SET(ObjectSetBasedRepository::new),
    INT2OBJECT_MAP(Int2ObjectMapBasedRepository::new),
    TINT_OBJECT_MAP(TIntObjectMapBasedRepository::new),
    HASH_INT_OBJ_MAP(HashIntObjMapBasedRepository::new);

    private final Supplier<InMemoryRepository<Order>> repositorySupplier;

    RepositoryType(Supplier<InMemoryRepository<Order>> repositorySupplier) {
        this.repositorySupplier = repositorySupplier;
    }

    public Supplier<InMemoryRepository<Order>> supplier() {
        return repositorySupplier;
    }
}
