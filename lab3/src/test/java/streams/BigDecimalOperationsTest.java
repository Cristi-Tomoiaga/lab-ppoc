package streams;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BigDecimalOperationsTest {

    @Test
    void testSum_empty() {
        List<BigDecimal> emptyList = new ArrayList<>();
        assertEquals(new BigDecimal(0), BigDecimalOperations.sum(emptyList));
    }

    @Test
    void testSum_oneElement() {
        List<BigDecimal> listOneElement = List.of(new BigDecimal(10));
        assertEquals(new BigDecimal(10), BigDecimalOperations.sum(listOneElement));
    }

    @Test
    void testSum_moreElements() {
        List<BigDecimal> listThreeElements = List.of(new BigDecimal(10),
                new BigDecimal(20), new BigDecimal(30));
        assertEquals(new BigDecimal(60), BigDecimalOperations.sum(listThreeElements));
    }

    @Test
    void testAverage_empty() {
        List<BigDecimal> emptyList = new ArrayList<>();
        assertEquals(new BigDecimal(0), BigDecimalOperations.average(emptyList));
    }

    @Test
    void testAverage_oneElement() {
        List<BigDecimal> listOneElement = List.of(new BigDecimal(10));
        assertEquals(new BigDecimal(10), BigDecimalOperations.average(listOneElement));
    }

    @Test
    void testAverage_moreElements() {
        List<BigDecimal> listThreeElements = List.of(new BigDecimal(10),
                new BigDecimal(20), new BigDecimal(30));
        assertEquals(new BigDecimal(20), BigDecimalOperations.average(listThreeElements));
    }

    @Test
    void testTopTenPercentBiggest_empty() {
        List<BigDecimal> emptyList = new ArrayList<>();
        assertEquals(0, BigDecimalOperations.topTenPercentBiggest(emptyList).size());
    }

    @Test
    void testTopTenPercentBiggest_threeElements() {
        List<BigDecimal> listThreeElements = List.of(new BigDecimal(10),
                new BigDecimal(20), new BigDecimal(30));
        List<BigDecimal> topTenPercentThreeElements = List.of(new BigDecimal(30));
        List<BigDecimal> computedTopTenPercentThreeElements
                = BigDecimalOperations.topTenPercentBiggest(listThreeElements);

        assertEquals(1, computedTopTenPercentThreeElements.size());
        assertEquals(topTenPercentThreeElements.get(0), computedTopTenPercentThreeElements.get(0));
    }

    @Test
    void testTopTenPercentBiggest_moreElements() {
        List<BigDecimal> listMoreElements = new ArrayList<>();
        for (int i = 1; i <= 40; i++) {
            listMoreElements.add(new BigDecimal(i * 10));
        }
        List<BigDecimal> topTenPercentMoreElements = new ArrayList<>();
        for (int i = 40; i >= 37; i--) {
            topTenPercentMoreElements.add(new BigDecimal(i * 10));
        }
        List<BigDecimal> computedTopTenPercentMoreElements
                = BigDecimalOperations.topTenPercentBiggest(listMoreElements);

        assertEquals(4, computedTopTenPercentMoreElements.size());
        assertEquals(topTenPercentMoreElements.get(0), computedTopTenPercentMoreElements.get(0));
        assertEquals(topTenPercentMoreElements.get(1), computedTopTenPercentMoreElements.get(1));
        assertEquals(topTenPercentMoreElements.get(2), computedTopTenPercentMoreElements.get(2));
        assertEquals(topTenPercentMoreElements.get(3), computedTopTenPercentMoreElements.get(3));
    }
}