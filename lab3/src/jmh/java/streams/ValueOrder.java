package streams;

public enum ValueOrder {
    RANDOM, ASCENDING, DESCENDING
}
