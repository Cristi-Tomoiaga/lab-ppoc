package streams;

import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@Fork(1)
@Measurement(iterations = 4, time = 2)
@Warmup(iterations = 2, time = 2)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class PrimitiveDoubleOperationsBenchmark {
    @State(Scope.Benchmark)
    public static class ValuesState {
        @Param({"100", "1000", "10000", "100000", "1000000"})
        public int size;

        @Param({"RANDOM", "ASCENDING", "DESCENDING"})
        public ValueOrder order;

        public double[] values;

        @Setup(Level.Trial)
        public void doSetup() {
            values = new double[size];

            switch (order) {
                case RANDOM:
                    Random random = new Random();
                    for (int i = 0; i < size; i++) {
                        values[i] = random.nextDouble() * 100000.0;
                    }

                    break;
                case ASCENDING:
                    for (int i = 1; i <= size; i++) {
                        values[i - 1] = i * 100.0;
                    }

                    break;
                case DESCENDING:
                    for (int i = size; i >= 1; i--) {
                        values[size - i] = i * 100.0;
                    }

                    break;
            }
        }
    }

    @Benchmark
    public double testSum(ValuesState valuesState) {
        return PrimitiveDoubleOperations.sum(valuesState.values);
    }

    @Benchmark
    public double testAverage(ValuesState valuesState) {
        return PrimitiveDoubleOperations.average(valuesState.values);
    }

    @Benchmark
    public double[] testTopTenPercentBiggest(ValuesState valuesState) {
        return PrimitiveDoubleOperations.topTenPercentBiggest(valuesState.values);
    }
}
