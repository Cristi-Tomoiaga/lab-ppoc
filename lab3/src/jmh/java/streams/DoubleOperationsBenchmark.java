package streams;

import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@Fork(1)
@Measurement(iterations = 4, time = 2)
@Warmup(iterations = 2, time = 2)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class DoubleOperationsBenchmark {
    @State(Scope.Benchmark)
    public static class ValuesState {
        @Param({"100", "1000", "10000", "100000", "1000000"})
        public int size;

        @Param({"RANDOM", "ASCENDING", "DESCENDING"})
        public ValueOrder order;

        public List<Double> values = new ArrayList<>();

        @Setup(Level.Trial)
        public void doSetup() {
            values.clear();

            switch (order) {
                case RANDOM:
                    Random random = new Random();
                    for (int i = 0; i < size; i++) {
                        values.add(random.nextDouble() * 100000.0);
                    }

                    break;
                case ASCENDING:
                    for (int i = 1; i <= size; i++) {
                        values.add(i * 100.0);
                    }

                    break;
                case DESCENDING:
                    for (int i = size; i >= 1; i--) {
                        values.add(i * 100.0);
                    }

                    break;
            }
        }
    }

    @Benchmark
    public Double testSum(ValuesState valuesState) {
        return DoubleOperations.sum(valuesState.values);
    }

    @Benchmark
    public Double testAverage(ValuesState valuesState) {
        return DoubleOperations.average(valuesState.values);
    }

    @Benchmark
    public List<Double> testTopTenPercentBiggest(ValuesState valuesState) {
        return DoubleOperations.topTenPercentBiggest(valuesState.values);
    }
}
