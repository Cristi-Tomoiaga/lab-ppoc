package streams;

import java.util.Arrays;

public class PrimitiveDoubleOperations {
    public static double sum(double[] array) {
        return Arrays.stream(array)
                .reduce(0.0, Double::sum);
    }

    public static double average(double[] array) {
        if (array.length == 0) {
            return 0.0;
        }

        return (Arrays.stream(array)
                .reduce(0.0, Double::sum)) / array.length;
    }

    public static double[] topTenPercentBiggest(double[] array) {
        return Arrays.stream(array)
                .sorted()
                .skip(array.length - (long) Math.ceil(array.length * 0.10))
                .toArray();
    }
}
