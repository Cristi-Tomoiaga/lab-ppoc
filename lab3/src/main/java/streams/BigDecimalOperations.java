package streams;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BigDecimalOperations {
    public static BigDecimal sum(List<BigDecimal> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal average(List<BigDecimal> list) {
        if (list.isEmpty()) {
            return new BigDecimal(0);
        }

        return list.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(list.size()), RoundingMode.UNNECESSARY);
    }

    public static List<BigDecimal> topTenPercentBiggest(List<BigDecimal> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.reverseOrder())
                .limit((long) Math.ceil(list.size() * 0.10))
                .collect(Collectors.toList());
    }
}
