package streams;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DoubleOperations {
    public static Double sum(List<Double> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .reduce(0.0, Double::sum);
    }

    public static Double average(List<Double> list) {
        if (list.isEmpty()) {
            return 0.0;
        }

        return (list.stream()
                .filter(Objects::nonNull)
                .reduce(0.0, Double::sum)) / list.size();
    }

    public static List<Double> topTenPercentBiggest(List<Double> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.reverseOrder())
                .limit((long) Math.ceil(list.size() * 0.10))
                .collect(Collectors.toList());
    }
}
